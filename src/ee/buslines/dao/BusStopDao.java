package ee.buslines.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import ee.buslines.BusStop;

/**
*	Bussipeatuse andmejuurdepääsu objekt
*/
public class BusStopDao extends HibernateDaoSupport {

	/**
	*	Meetod kõikide bussipeatuste saamiseks andmebaasist
	*	@return List bussipeatustega
	*	@throws DataAccessException
	*/
	@SuppressWarnings("unchecked")
	public List<BusStop> getBusStops() throws DataAccessException {
		return getHibernateTemplate().find("from BusStop stop order by stop.name");
	
	}
	
	/**
	*	Meetod bussipeatuse võtmiseks andmebaasist unikaalse id järgi
	*	@param id Id
	*	@return Id'le vastav bussipeatus
	*	@throws DataAccessException
	*/
	public BusStop loadBusStop(int id) throws DataAccessException{
		return (BusStop)getHibernateTemplate().load(BusStop.class, new Integer(id));

	}
	
	/**
	*	Meetod bussipeatuse salvestamiseks andmebaasi
	*	@param busStop Bussipeatus
	*	@throws DataAccessException
	*/
	public void storeBusStop(BusStop busStop) throws DataAccessException{
		getHibernateTemplate().saveOrUpdate(busStop);
	}
	
	/**
	*	Meetod bussipeatuse eemaldamiseks andmebaasist id järgi
	*	@param id Id
	*	@throws DataAccessException
	*/
	public void removeBusStop(int id) throws DataAccessException{
		BusStop busStop = loadBusStop(id);
		getHibernateTemplate().delete(busStop);
	}
	
}
	
