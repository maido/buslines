package ee.buslines.manager;

import java.util.List;

import ee.buslines.BusLine;
import ee.buslines.BusStop;
import ee.buslines.dao.BusLineDao;
import ee.buslines.dao.BusStopDao;

/**
*	Manager bussiliinide ja peatuste andmejuurdepääsu objektidele
*/
public class Manager {

	/**
	*	Bussiliini andmejuurdepääsu objekt
	*/
	private BusLineDao busLineDao;
	
	/**
	*	Bussipeatuse andmejuurdepääsu objekt
	*/
	private BusStopDao busStopDao;
	
	/**
	*	Meetod bussiliini andmejuurdepääsu objekti võtmiseks
	*	@return Bussiliini andmejuurdepääsu objekt
	*/
	public BusLineDao getBusLineDao() {
		return busLineDao;
	}
	
	/**
	*	Meetod bussiliini andmejuurdepääsu objekti sättimiseks
	*	@param busLineDao Bussiliini andmejuurdepääsu objekt
	*/
	public void setBusLineDao(BusLineDao busLineDao) {
		this.busLineDao = busLineDao;
	}
	
	/**
	*	Meetod bussipeatuse andmejuurdepääsu objekti võtmiseks
	*	@return Bussipeatuse andmejuurdepääsu objekt
	*/
	public BusStopDao getBusStopDao() {
		return busStopDao;
	}
	
	/**
	*	Meetod bussipeatuse andmejuurdepääsu objekti sättimiseks
	*	@param busStopDao Bussipeatuse andmejuurdepääsu objekt
	*/
	public void setBusStopDao(BusStopDao busStopDao) {
		this.busStopDao = busStopDao;
	}
	
	/**
	*	Meetod bussiliini võtmiseks id järgi
	*	@param id Id
	*	@return Bussiliin
		@see BusLineDao#loadBusLine(int)
	*/
	public BusLine getBusLineById(int id) {
		return busLineDao.loadBusLine(id);
	}
	
	/**
	*	Meetod bussiliini salvestamiseks
	*	@param busLine Bussiliin
	*	@return Bussiliini andmejuurdepääsu objekt
	*	@see BusLineDao#saveBusLine(BusLine)
	*/
	public BusLineDao saveBusLine(BusLine busLine) {
		busLineDao.saveBusLine(busLine);
		return busLineDao;
	}
	
	/**
	*	Meetod kõikide bussiliinide saamiseks
	*	@return List bussiliinidega
	*	@see BusLineDao#getBusLines()
	*/
	public List<BusLine>getBusLines() {
		return busLineDao.getBusLines();
	}
	
	/**
	*	Meetod bussiliini eemaldamiseks id järgi
	*	@param id Id
	*	@see BusLineDao#removeBusLine(int)
	*/
	public void removeBusLine(int id) {
		busLineDao.removeBusLine(id);
	}
	
	/**
	*	Meetod bussipeatuste saamiseks
	*	@return List bussipeatustega
	*	@see BusStopDao#getBusStops()
	*/
	public List<BusStop> getBusStops() {
		return busStopDao.getBusStops();
	}
	
	/**
	*	Meetod bussipeatuse saamiseks id järgi
	*	@param id Id
	*	@return Bussipeatus
	*	@see BusStopDao#loadBusStop(int)
	*/
	public BusStop getBusStopById(int id) {
		return busStopDao.loadBusStop(id);
		
	}
	
	/**
	*	Meetod bussipeatuse eemaldamiseks id järgi
	*	@param id Id
	*	@see BusStopDao#removeBusStop(int)
	*/
	public void removeBusStop(int id) {
		busStopDao.removeBusStop(id);
	}
	
	/**
	*	Meetod bussipeatuse salvestamiseks
	*	@param busStop Bussipeatus
	*	@return Bussipeatuse andmejuurdepääsu objekt
	*	@see BusStopDao#storeBusStop(BusStop)
	*/
	public BusStopDao saveBusStop(BusStop busStop) {
		busStopDao.storeBusStop(busStop);
		return busStopDao;
	}
		

}
