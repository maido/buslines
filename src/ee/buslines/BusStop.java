package ee.buslines;

/**
*	Bussipeatuse objekt
*/
public class BusStop implements Comparable<BusStop>{
	
	/**
	*	Bussipeatuse unikaalne id
	*/
	private int id = 0;
	
	/**
	*	Bussipeatuse nimi
	*/
	private String name;
	
	/**
	*	Meetod bussipeatuse id saamiseks
	*	@return Bussipeatuse id
	*/
	public int getId() {
		return id;
	}
	
	/**
	*	Meetod bussipeatuse id sättimiseks
	*	@param id Uus id
	*/
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	*	Meetod bussipeatuse nime võtmiseks
	*	@return Bussipeatuse nimi
	*/
	public String getName() {
		return name;
	}
	
	/**
	*	Meetod bussipeatuse nime sättimiseks
	*	@param name Uus nimi
	*/
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	*	Meetod, mis lubab bussipeatusi omavahel võrrelda
	*	@param stop Võrreldav bussipeatus
	*	@return positiivne, kui bussipeatuse nimi on võrreldavast tähestikulises järjekorras eespool, negatiivne kui tagapool ja 0 kui võrdne
	*/
	public int compareTo(BusStop stop) {
		return name.compareTo(stop.getName());
	}
	
}
