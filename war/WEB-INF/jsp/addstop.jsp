<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css"/>
		<title>Bussiliinid</title>
	</head>
	<body>
		<div id="wrapper">
			<%@include file="/WEB-INF/jsp/inc/header.jsp" %>
			<%@include file="/WEB-INF/jsp/inc/menu.jsp" %>
			<div id="content" >
				<h3>Peatuse lisamine liini nr <c:out value="${linenr}"/></h3>
				<c:choose>
					<c:when test="${not(empty stop.stops)}">
						<form method="post" action="buslines.htm">
						<select name="stop" id="stop">
							<c:forEach items="${stop.stops}" var="stops">
								<option value="${stops.id}">${stops.name}</option>
							</c:forEach>
						</select>
						<input type="hidden" name="method" id="method" value="addStopToLine"/>
						<input type="hidden" name="line" id="line" value="${lineadd}">
						<input  type="submit" value="Lisa" />
   						</form>
   					</c:when>
   					<c:otherwise>
   						<span class="error">Ei ole rohkem peatusi, mida liinile lisada. Peatuste lisamiseks vali men��st Bussipeatuste lisamine</span>
   					</c:otherwise>
   				</c:choose>
			</div>
			<%@include file="/WEB-INF/jsp/inc/footer.jsp" %>
		</div>
	</body>
</html>